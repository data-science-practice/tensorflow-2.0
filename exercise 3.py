# https://stackoverflow.com/questions/49052783/expected-binary-or-unicode-string-got-24-tensorflow-dataset-numerical-categori

import tensorflow as tf
import pandas as pd

BATCH_SIZE = 32

d = {
    'brand': ["Sony", "Samsung", "Sony", "Sony", "Samsung"], 
    'n2': [24,20,18,26,24],
    'n3': [1,0,0,1,1]
    }
features = pd.DataFrame(data=d)
labels = [0,1,0,0,1]

feature_columns = [
    tf.feature_column.categorical_column_with_hash_bucket("brand", hash_bucket_size=5),
    tf.feature_column.numeric_column(key="n2", dtype=tf.float32),
    tf.feature_column.numeric_column(key="n3", dtype=tf.float32)
]

def create_input_fn(features, labels, shuffle=False, epochs=1):
    def input_fn():
        train_dataset = tf.data.Dataset.from_tensor_slices((dict(features), labels))
        if shuffle:
            train_dataset = train_dataset.shuffle(1000)
        return train_dataset.repeat(epochs).batch(BATCH_SIZE)
    return input_fn

classifier = tf.estimator.DNNClassifier(
   feature_columns=feature_columns, 
   hidden_units=[40, 60, 30, 12],
   n_classes=2,
   model_dir="./") # Path to where checkpoints etc are stored

classifier = tf.estimator.LinearClassifier(feature_columns=feature_columns)

classifier.train(input_fn=create_input_fn(features, labels, True, 1))

print("Done")