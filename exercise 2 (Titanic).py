import pandas as pd
import numpy as np
import tensorflow as tf

# data obtained from https://www.kaggle.com/c/titanic-dataset/data
data = pd.read_csv("./data/titanic_train.csv") # if needed use "na_values=..." to define which values are "nan"

## Normalize values
#print(data["age"].unique())
# normalize "age", it contains values lower then 1 and valus with decimals
data["age"].mask(data["age"] < 1.0, other=0, inplace=True) # set to "0" values lower than 1
data["age"].fillna(0, inplace=True)
data["age"] = data["age"].map(lambda x: int(x)) # convert to int
# print(data["age"].unique())

data["fare"].fillna(0, inplace=True)
print(data["fare"].unique())

#data.assign(temp_c = lambda x: 1 if (x["sibsp"] + x["parch"]) == 0 else 0) # does noty work
#data = data.assign(temp_c = lambda x: get_alone(x))  # ValueError: The truth value of a Series is ambiguous. 
data["alone"] = data.apply(lambda x: 1 if (x.sibsp + x.parch) == 0 else 0, axis=1)

rows_count = data.shape[0] # shape returns a tuple of the number of rows and columns
print("The number of rows in the training data is: ", rows_count)
# get 20% of the data for testing
split_index = int(rows_count * 0.2)
test_data = data.iloc[:split_index]
train_data = data.iloc[split_index:]
# resulting sliced dataframes maintain the same index (the train_data index starts at split_index)

#print(train_data.head())
#reindex = range(0,split_index)
#train_data.reindex(reindex)
#print(train_data.head())
# reindex() does not change the index of the dataframe
# reset_index() creates a new "index" column with the old index values

print(f"Training data size: {train_data.shape[0]}")
print(f"Test data size: {test_data.shape[0]}")

print(train_data.head()) # equivalent to train_data.loc[0:4] or iloc[0:4]
print(test_data.head())
print(train_data.describe())

# look at column "survived" and age
#print(train_data["survived"])
#print(train_data["age"])

print(test_data.loc[0])
print(train_data.iloc[0])

# Create the labels
train_result = train_data.pop("survived")  # extract the label "survived" (it is removed from the dataframe)
test_result = test_data.pop("survived")

## Create the features
categorical_columns = ["pclass", "sex", "alone"]  # "alone"
numerical_columns = ["fare", "age"] # "sibsp", "parch", "age", "fare"
train_data = train_data[categorical_columns + numerical_columns]
test_data = test_data[train_data.columns]

feature_columns = []
for feature_name in categorical_columns:
    vocabulary = train_data[feature_name].unique()
    feature_columns.append(tf.feature_column.categorical_column_with_vocabulary_list(feature_name, vocabulary))

for feature_name in numerical_columns:
    feature_columns.append(tf.feature_column.numeric_column(feature_name, dtype=tf.float32))

## Create input functions and estimator
def create_input_function(input_data, result_data, epochs=1, shuffle=False, batch_size=32):
    def input_function():
        dataset = tf.data.Dataset.from_tensor_slices((dict(input_data), result_data))  # tuple with feature and label
        if shuffle:
            dataset = dataset.shuffle(1000)
        dataset = dataset.repeat(epochs).batch(batch_size)
        return dataset
    return input_function

epochs = 10
repetition = 3
train_input_fn = create_input_function(train_data, train_result, epochs=epochs, shuffle=True)
test_input_fn = create_input_function(test_data, test_result)

print(f"\nFeatures: {train_data.columns.values}")
print(f"Epochs: {epochs}")
results = []
linear_estimator = tf.estimator.LinearClassifier(feature_columns=feature_columns)
for x in range(0,repetition):    
    linear_estimator.train(train_input_fn)
    result = linear_estimator.evaluate(test_input_fn)
    results.append(result)
    print(f"Test Accuracy: {result['accuracy']:.3f}")

# results_values = list(map(lambda result: f"{result['accuracy']:.3f}", results))
results_values = list(map(lambda result: result['accuracy'], results))
print(f"Avg Accuracy:  {np.mean(results_values):.3f} ({results_values})")
print(f"{str(train_data.columns.values)[1:-1]} | {epochs} | {np.mean(results_values):.3f} {list(map(lambda x: f'{x:.3f}', results_values))}")


## Check probabilities
prediction = list(linear_estimator.predict(test_input_fn))
#print(f"Prediction: {prediction}")
index_to_check = 53
print(f"\nChecking probabilities for index {index_to_check}")
print(test_data.iloc[index_to_check])
print("")
print(f"Our prediction they survive: {prediction[index_to_check]['probabilities'][1]*100.:.1f}%")
print(f"Did they survive? {'y' if test_result.iloc[index_to_check] == 1 else 'n'}")

