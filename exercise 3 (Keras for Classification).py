import pandas as pd
import tensorflow as tf

# load the dataset
ds = pd.read_csv("./data/iris.csv")
#print(ds.head())


# normalization
# create a "Species" column with the values "setosa", "versicolor", "virginica" as integers
#ds["species"] = ds.apply(lambda x: "setosa" if x["species"] == "Iris-setosa" else "versicolor" if x["species"] == "Iris-versicolor" else "virginica", axis=1)
SPECIES = ["setosa", "versicolor", "virginica"]
ds["species"] = ds.apply(lambda x: 0 if x["species"] == "Iris-setosa" else 1 if x["species"] == "Iris-versicolor" else 2, axis=1)
#print(ds.head())

training_percentage = 90
total_rows = ds.shape[0]
training_rows = int(total_rows * training_percentage / 100)

#train_ds = ds.sample(frac=training_percentage/100)
train_ds = ds.iloc[:training_rows]
#print(f"Training rows: {train_ds.shape[0]}")
print(f"Training rows: {len(train_ds)}")
test_ds = ds.drop(train_ds.index)
print(f"Test rows: {test_ds.shape[0]}")

train_result = train_ds.pop("species")
test_result = test_ds.pop("species")

# create input function
def input_fn(features, labels, training=True, batch_size=256):
    dataset = tf.data.Dataset.from_tensor_slices((dict(features), labels))
    if training:
        dataset.shuffle(len(features)).repeat()    
    return dataset.batch(batch_size)

# create features and labels
features = list(map(lambda k: tf.feature_column.numeric_column(k), train_ds.keys()))
#print(features)
labels = ["species"]

# create Classifiction model
hidden_layers = [30, 10]
steps = 5000
classifier = tf.estimator.DNNClassifier(
    feature_columns=features,
    hidden_units=hidden_layers,
    n_classes=3)


classifier.train(input_fn=lambda: input_fn(train_ds, train_result, training=True), steps=steps)
result = classifier.evaluate(input_fn=lambda: input_fn(test_ds, test_result, training=False))
print(f"Layers: {hidden_layers} | Steps: {steps} | {result}")
print(f"Accuracy {result['accuracy']:.3f}")



# Prediction
def prediction_input_fn(features):
    return tf.data.Dataset.from_tensor_slices(dict(features)).batch(256)

prediction_features = {}
for feature in train_ds.keys():
    ask = True
    while ask:
        try:
            val = float(input(f"{feature}: "))
            prediction_features[feature] = [val] # we pass only a "row" but it still expect a list of values
            ask = False
        except:
            pass

prediction = classifier.predict(input_fn=lambda: prediction_input_fn(prediction_features))
for predictoin_value in prediction: # with a single "row" we actually have only one prediction
    class_id = predictoin_value["class_ids"][0]
    probability = predictoin_value["probabilities"][class_id]
    print(f"Prediction: {SPECIES[class_id]} ({probability*100:.3f}%)")