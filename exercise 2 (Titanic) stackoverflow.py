# https://stackoverflow.com/questions/70493732/nanlossduringtrainingerror-in-tensorflow-why-does-it-happen-how-to-solve

import pandas as pd
import tensorflow as tf

# data obtained from https://www.kaggle.com/c/titanic-dataset/data
data = pd.read_csv("./data/titanic_train.csv") 
print(data["age"].unique()) # some "nan"

train_data = data

# Create the labels
train_result = train_data.pop("survived") 

## Create feature columns
categorical_columns = ["pclass", "sex"]
numerical_columns = ["sibsp", "parch", "fare", "age"]
train_data = train_data[categorical_columns + numerical_columns]

feature_columns = []
for feature_name in categorical_columns:
    vocabulary = train_data[feature_name].unique()
    feature_columns.append(tf.feature_column.categorical_column_with_vocabulary_list(feature_name, vocabulary))

for feature_name in numerical_columns:
    feature_columns.append(tf.feature_column.numeric_column(feature_name, dtype=tf.float32))

def create_input_function(input_data, result_data, epochs=10, shuffle=True, batch_size=32):
    def input_function():
        # the error is raised here
        dataset = tf.data.Dataset.from_tensor_slices((dict(input_data), result_data)) 
        if shuffle:
            dataset = dataset.shuffle(1000)
        dataset = dataset.repeat(epochs).batch(batch_size)
        return dataset
    return input_function

train_input_fn = create_input_function(train_data, train_result)

linear_estimator = tf.estimator.LinearClassifier(feature_columns=feature_columns)

linear_estimator.train(train_input_fn)