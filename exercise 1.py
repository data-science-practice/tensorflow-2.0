import tensorflow as tf
import numpy
from tensorflow.python.framework.ops import tensor_id

print("Exercise 1")
print(f"tensorflow version: {tf.__version__}")

# define some tesnosorflow scalar variables
string = tf.Variable("this is a string", tf.string)
number = tf.Variable(75, tf.int16)
float_number = tf.Variable(1.23, tf.float64)

# define some vactor (arrays) variables of Rank 1 and 2
rank1_string = tf.Variable(["this", "is", "a", "string"], tf.string)
rank2_string = tf.Variable([["this", "is", "a", "string"], ["this", "is", "a", "string"], ["a", "b", "c", "d"]], tf.string)
print(f"1 shape: {tf.shape(rank1_string)}")  
print(f"1 rank: {tf.rank(rank1_string)}")
print(f"2 shape: {tf.shape(rank2_string)}")
print(f"2 rank: {tf.rank(rank2_string)}")

def tensor_info(tensor_name, tensor): print(f"{tensor_name} shape: {tf.shape(tensor)}")

# change shape
tensor_1 = tf.Variable([1, 2, 3, 4], tf.int16)
tensor_2 = tf.reshape(tensor_1, [4, 1])
tensor_info("tensor_1", tensor_1)
tensor_info("tensor_2", tensor_2)

tensor_3 = tf.zeros([1,2,3], tf.int16)
tensor_4 = tf.reshape(tensor_3, [3, -1]) # -1 means "as many as possible"
tensor_info("tensor_3", tensor_3)
print(tensor_3)
tensor_info("tensor_4", tensor_4)
print(tensor_4)

print("end")

